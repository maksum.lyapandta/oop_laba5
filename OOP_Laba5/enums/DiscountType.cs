﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba5.enums
{
    enum DiscountType
    {
        None,
        Student,
        SeniorCitizen,
        Employee
    }
}
