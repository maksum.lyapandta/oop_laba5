﻿using OOP_Laba5.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba5.structs
{
    struct PhoneCallPayment
    {
        public string SubscriberName;
        public string PhoneNumber;
        public DateTime CallDate;
        public decimal MinuteRate;
        public int CallDuration;
        public DiscountType Discount;
        public SubscriberType Type;

        public decimal TotalPayment
        {
            get
            {
                decimal total = MinuteRate * CallDuration;
                if (Discount == DiscountType.Student)
                {
                    total *= 0.9m;
                }
                else if (Discount == DiscountType.SeniorCitizen)
                {
                    total *= 0.8m; 
                }
                else if (Discount == DiscountType.Employee)
                {
                    total *= 0.7m; 
                }
                return total;
            }
        
    }
    }
}
