﻿using OOP_Laba5.structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba5;

 class CallInfo
{
 public  static void SearchAndDisplayBySubscriberName(List<PhoneCallPayment> payments, string subscriberName)
    {
        var results = payments.FindAll(p => p.SubscriberName == subscriberName);
        if (results.Count == 0)
        {
            Console.WriteLine("Немає даних, що відповідають вашому запиту.");
        }
        else
        {
            Console.WriteLine("Результати пошуку за прізвищем абонента:");
            DisplayPhoneCallPayments(results);
        }
       
    }

  public  static void SearchAndDisplayByPhoneNumber(List<PhoneCallPayment> payments, string phoneNumber)
    {
        var results = payments.FindAll(p => p.PhoneNumber == phoneNumber);
        if (results.Count == 0)
        {
            Console.WriteLine("Немає даних, що відповідають вашому запиту.");
        }
        else
        {
            Console.WriteLine("Результати пошуку за номером телефону:");
            DisplayPhoneCallPayments(results);
        }
    }

  public  static void SearchAndDisplayByCallDate(List<PhoneCallPayment> payments, DateTime callDate)
    {
        var results = payments.FindAll(p => p.CallDate.Date == callDate.Date);
        if (results.Count == 0)
        {
            Console.WriteLine("Немає даних, що відповідають вашому запиту.");
        }
        else
        {
            Console.WriteLine("Результати пошуку за датою дзвінка:");
            DisplayPhoneCallPayments(results);
        }
    }

   public static void DisplayPhoneCallPayments(List<PhoneCallPayment> payments)
    {
        foreach (var payment in payments)
        {
            Console.WriteLine($"Прізвище абонента: {payment.SubscriberName}");
            Console.WriteLine($"Номер телефону: {payment.PhoneNumber}");
            Console.WriteLine($"Дата дзвінка: {payment.CallDate}");
            Console.WriteLine($"Тариф за хвилину розмови: {payment.MinuteRate:C2}");
            Console.WriteLine($"Тривалість дзвінка: {payment.CallDuration} хв.");
            Console.WriteLine($"Пільгова знижка: {payment.Discount}%");
            Console.WriteLine($"Тип абонента: {payment.Type}");
            Console.WriteLine($"Загальна сума: {payment.TotalPayment:C2}");
            Console.WriteLine("=====================================");
        }
    }

   public static void SaveDataToFile(List<PhoneCallPayment> payments, string fileName)
    {
        using (StreamWriter writer = new StreamWriter(fileName))
        {
            foreach (var payment in payments)
            {
                writer.WriteLine($"{payment.SubscriberName}|{payment.PhoneNumber}|{payment.CallDate}|{payment.MinuteRate}|{payment.CallDuration}|{payment.Discount}|{payment.Type}");
            }
        }
    }
}
