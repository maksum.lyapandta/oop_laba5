﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using OOP_Laba5.enums;
using OOP_Laba5.structs;
using OOP_Laba5;

class Program
{
    static void Main()
    {
        string fileName = "phone_calls.txt";
        
        List<PhoneCallPayment> phoneCallPayments = new List<PhoneCallPayment>();


        if (File.Exists(fileName))
        {
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string[] parts = reader.ReadLine().Split('|');
                    if (parts.Length == 7)
                    {
                        PhoneCallPayment payment = new PhoneCallPayment
                        {
                            SubscriberName = parts[0],
                            PhoneNumber = parts[1],
                            CallDate = DateTime.Parse(parts[2]),
                            MinuteRate = decimal.Parse(parts[3]),
                            CallDuration = int.Parse(parts[4]),
                            Discount = (DiscountType)Enum.Parse(typeof(DiscountType), parts[5]),
                            Type = (SubscriberType)Enum.Parse(typeof(SubscriberType), parts[6])
                        };
                        phoneCallPayments.Add(payment);
                    }
                }
            }
        }

        while (true)
        {
            Console.WriteLine("Меню:");
            Console.WriteLine("1. Додати інформацію про платіж за дзвінок");
            Console.WriteLine("2. Пошук за прізвищем абонента");
            Console.WriteLine("3. Пошук за номером телефону");
            Console.WriteLine("4. Пошук за датою дзвінка");
            Console.WriteLine("5. Вивести всі записи");
            Console.WriteLine("6. Вийти");

            Console.Write("Оберіть опцію: ");
            string choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    PhoneCallPayment newPayment = ReadPhoneCallPaymentFromUser();
                    phoneCallPayments.Add(newPayment);
                    break;
                case "2":
                    Console.Write("Введіть прізвище абонента: ");
                    string subscriberName = Console.ReadLine();
                   CallInfo.SearchAndDisplayBySubscriberName(phoneCallPayments, subscriberName);
                    break;
                case "3":
                    Console.Write("Введіть номер телефону: ");
                    string phoneNumber = Console.ReadLine();
                    CallInfo.SearchAndDisplayByPhoneNumber(phoneCallPayments, phoneNumber);
                    break;
                case "4":
                    Console.Write("Введіть дату дзвінка (у форматі yyyy-MM-dd): ");
                    if (DateTime.TryParse(Console.ReadLine(), out DateTime callDate))
                    {
                        CallInfo.SearchAndDisplayByCallDate(phoneCallPayments, callDate);
                    }
                    else
                    {
                        Console.WriteLine("Неправильний ввід.");
                    }
                    break;
                case "5":
                    CallInfo.DisplayPhoneCallPayments(phoneCallPayments);
                    break;
                case "6":
                    CallInfo.SaveDataToFile(phoneCallPayments, fileName);
                    return;
                default:
                    Console.WriteLine("Неправильний ввід.");
                    break;
            }
        }
    }

    static PhoneCallPayment ReadPhoneCallPaymentFromUser()
    {
        PhoneCallPayment payment = new PhoneCallPayment();

        Console.Write("Прізвище абонента: ");
        payment.SubscriberName = Console.ReadLine();

        Console.Write("Номер телефону: ");
        payment.PhoneNumber = Console.ReadLine();

        Console.Write("Дата дзвінка (у форматі yyyy-MM-dd): ");
        payment.CallDate = DateTime.Parse(Console.ReadLine());

        Console.Write("Тариф за хвилину розмови: ");
        payment.MinuteRate = decimal.Parse(Console.ReadLine());

        Console.Write("Тривалість дзвінка (у хвилинах): ");
        payment.CallDuration = int.Parse(Console.ReadLine());

        Console.Write("Тип знижки (None, Student, SeniorCitizen, Employee): ");
        if (Enum.TryParse(Console.ReadLine(), out DiscountType discount))
        {
           
            payment.Discount = discount;
        }
        else
        {
            Console.WriteLine("Неправильний ввід. За замовчуванням встановлено None.");
            payment.Discount = DiscountType.None;
        }
       
        Console.Write("Тип абонента (Regular або Premium): ");
        if (Enum.TryParse(Console.ReadLine(), out SubscriberType type))
        {
            payment.Type = type;
        }
        else
        {
            Console.WriteLine("Неправильний ввід. За замовчуванням встановлено Regular.");
            payment.Type = SubscriberType.Regular;
        }

        return payment;
    }

  
}
